import { LightningElement,wire,track } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import ACCOUNTSOURCE_FIELD from '@salesforce/schema/Account.AccountSource';


export default class DualListBoxLWCDemo extends LightningElement {
    @track _selected = []; 
    
    @wire(getObjectInfo, { objectApiName: ACCOUNT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: ACCOUNTSOURCE_FIELD})
    AccountSourceValues;

    get selected() {
        return this._selected.length ? this._selected : 'none';
    }

    handleChange(event) {
        this._selected = event.detail.value;
    }
}